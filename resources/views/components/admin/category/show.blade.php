<x-admin>
        <div class="form-group">
            <label for="product_name" class="form-label">Name</label>
            <input type="text" id="product_name" name="name" class="form-control" value="{{ $category->name }}" disabled>
        </div>
        <div class="form-group">
            <img src="{{ asset('storage/category/'.$category->image) }}" height="200px" alt="Category Image">
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($category->products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->price }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
</x-admin>
