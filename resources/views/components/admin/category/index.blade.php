<x-admin>
    <div class="actionButtons mb-3">
        <a href="#" class="btn btn-sm btn-primary">New</a>
        <a href="#" class="btn btn-sm btn-outline-danger">Trash</a>
    </div>
    @if (session('message'))
        <div class="alert">
            {{ session('message') }}
        </div>
    @endif
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>
                        <a href="{{ route('categories.show', $category->id) }}" class="btn btn-sm btn-outline-info">Show</a>
                        <a href="#" class="btn btn-sm btn-outline-secondary">Edit</a>
                        <a href="#" class="btn btn-sm btn-outline-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</x-admin>
