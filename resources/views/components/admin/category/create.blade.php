<x-admin>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('products.admin_store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="product_name" class="form-label">Name</label>
            <input type="text" id="product_name" name="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="product_description" class="form-label">Description</label>
            <textarea name="description" id="product_description" class="form-control" cols="30" rows="10"></textarea>
        </div>
        <div class="form-group">
            <label for="product_price" class="form-label">Price</label>
            <input type="number" step="any" id="product_price" name="price" class="form-control">
        </div>
        <div class="form-group">
            <label for="product_image" class="form-label">Choose an image</label>
            <input class="form-control" type="file" id="product_image" name="image">
        </div>
        <button class="btn btn-primary" type="submit">Save</button>
    </form>
</x-admin>
