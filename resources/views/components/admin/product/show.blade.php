<x-admin>
    <div class="form-group">
        <label for="product_name" class="form-label">Name</label>
        <input type="text" id="product_name" name="name" class="form-control" value="{{ $product->name }}" disabled>
    </div>
    <div class="form-group">
        <label for="product_description" class="form-label">Product Description</label><br>
        {!! $product->description !!}
    </div>
    <div class="form-group">
        <label for="product_price" class="form-label">Price</label>
        <input type="number" step="any" id="product_price" name="price" class="form-control"
            value="{{ $product->price }}" disabled>
    </div>
    <div class="form-group">
        <label for="product_category" class="form-label">Category</label>
        <input type="text" step="any" id="product_category" name="category" class="form-control"
            value="{{ $product->category->name }}" disabled>
    </div>
    <div class="form-group">
        <label for="product_brand" class="form-label">Brand</label>
        <input type="text" step="any" id="product_brand" name="brand" class="form-control"
            value="{{ $product->brand->name }}" disabled>
    </div>
    <div class="form-group">
        <label for="product_color" class="form-label">Color</label>
        <input type="text" step="any" id="product_color" name="color" class="form-control"
            value="{{ $product->color->name }}" disabled>
    </div>
    <div class="form-group">
        <label for="product_image" class="form-label">Choose an image</label>
        <img src="{{ asset('storage/products/' . $product->image) }}" alt="Product Image">
        <input class="form-control" type="file" id="product_image" name="image" value="{{ $product->image }}" disabled>
    </div>

    <h4>Product Orders</h4>
    <table class="table">
        <thead>
            <th>ID</th>
            <th>Address</th>
        </thead>
        <tbody>
            @foreach ($product->orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->address }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</x-admin>
