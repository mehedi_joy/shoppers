<x-admin>
    <div class="actionButtons mb-3">
        <a href="{{ route('products.admin_create') }}" class="btn btn-sm btn-primary">New</a>
        <a href="" class="btn btn-sm btn-outline-danger">Trash</a>
    </div>
    @if (session('message'))
        <div class="alert">
            {{ session('message') }}
        </div>
    @endif
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->price }}</td>
                    <td>
                        <a href="{{ route('products.admin_show', $product->id) }}" class="btn btn-sm btn-outline-primary">Show</a>
                        <a href="{{ route('products.admin_edit', $product->id) }}" class="btn btn-sm btn-outline-info">Edit</a>
                        <a href="#" class="btn btn-sm btn-outline-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</x-admin>
