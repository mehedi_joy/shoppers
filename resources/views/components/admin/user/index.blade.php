<x-admin>
    @if (session('message'))
        <div class="alert">
            {{ session('message') }}
        </div>
    @endif
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ ucfirst($user->role->name) }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <a href="{{ route('users.admin_edit_role', $user->id) }}" class="btn btn-sm btn-outline-secondary">Update Role</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</x-admin>
