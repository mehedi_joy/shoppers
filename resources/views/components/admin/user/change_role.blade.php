<x-admin>
    <form action="{{ route('users.admin_update_role', $user->id) }}" method="post">
        @csrf
        @method('patch')
        <div class="mb-3">
            <label for="role_select" class="form-label">Select Role</label>
            <select name="role_id" id="role_select" class="form-control">
                @foreach ($data as $role)
                <option value="{{ $role->id }}" @if ($user->role->id == $role->id) selected @endif>{{ $role->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</x-admin>
