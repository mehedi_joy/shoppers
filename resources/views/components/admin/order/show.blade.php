<x-admin>
        <div class="form-group">
            <label for="order_address" class="form-label">Address</label>
            <input type="text" id="order_address" name="name" class="form-control" value="{{ $order->address }}" disabled>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Product Name</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($order->products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
</x-admin>
