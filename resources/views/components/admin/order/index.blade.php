<x-admin>
    <h1>Orders</h1>
    <div class="actionButtons mb-3">
        <a href="{{ route('orders.admin_create') }}" class="btn btn-sm btn-primary">New</a>
        <a href="#" class="btn btn-sm btn-outline-danger">Trash</a>
    </div>
    @if (session('message'))
        <div class="alert">
            {{ session('message') }}
        </div>
    @endif
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Products</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>
                        @foreach ($order->products as $item)
                            {{ $item->id }}
                        @endforeach
                    </td>
                    <td>
                        <a href="{{ route('orders.show', $order->id) }}" class="btn btn-sm btn-outline-info">Show</a>
                        <a href="#" class="btn btn-sm btn-outline-secondary">Edit</a>
                        <a href="#" class="btn btn-sm btn-outline-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</x-admin>
