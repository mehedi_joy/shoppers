<x-admin>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('orders.admin_store') }}" method="post">
        @csrf
        @method('post')
        <div class="form-group">
            <label for="order_address" class="form-label">Address</label>
            <input type="text" id="order_address" name="address" class="form-control">
        </div>

        <div class="form-group">
            <label for="product_select">Select Products:</label>
            <select name="products[]" id="product_select" class="form-control" multiple>Products
                @foreach ($products as $product)
                <option value="{{ $product->id }}">{{ $product->name }}</option>
                @endforeach
            </select>
        </div>


        <button class="btn btn-primary" type="submit">Save</button>
    </form>
</x-admin>
