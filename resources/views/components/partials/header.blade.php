<header class="site-navbar" role="banner">
    <div class="site-navbar-top">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-6 col-md-4 order-2 order-md-1 site-search-icon text-left">
                    <form action="{{ route('products.search') }}" class="site-block-top-search">
                        @csrf
                        <span class="icon icon-search2"></span>
                        <input name="name" type="text" class="form-control border-0" placeholder="Search">
                    </form>
                </div>

                <div class="col-12 mb-3 mb-md-0 col-md-4 order-1 order-md-2 text-center">
                    <div class="site-logo">
                        <a href="{{ route('index') }}" class="js-logo-clone">Shoppers</a>
                    </div>
                </div>

                <div class="col-6 col-md-4 order-3 order-md-3 text-right">
                    <div class="site-top-icons">
                        <ul>
                            @auth
                            <li><a href="#"><span class="icon icon-person"></span></a></li>
                            <li><a href="#"><span class="icon icon-heart-o"></span></a></li>
                            <li>
                                <a href="{{ route('cart') }}" class="site-cart">
                                    <span class="icon icon-shopping_cart"></span>
                                    <span class="count">2</span>
                                </a>
                            </li>
                            <li>
                                <form action="{{ route('logout') }}" method="POST">
                                    @csrf
                                    @method('post')
                                    <input class="btn btn-sm mb-2" type="submit" value="Log Out">
                                </form>
                            </li>
                            @else
                            <li><a class="btn btn-sm btn-outline-info" href="{{ route('login') }}">Login</a></li>
                            <li><a class="btn btn-sm btn-outline-info" href="{{ route('register') }}">Register</a></li>
                            @endauth
                            <li class="d-inline-block d-md-none ml-md-0"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu"></span></a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <nav class="site-navigation text-right text-md-center" role="navigation">
        <div class="container">
            <ul class="site-menu js-clone-nav d-none d-md-block">
                <li class="active"><a href="{{ route('index') }}">Home</a></li>
                <li class="has-children">
                    <a href="#">Categories</a>
                    <ul class="dropdown">
                        {{-- <li><a href="#">Menu One</a></li> --}}
                        @foreach ($categories as $category )
                        <li><a href="{{ route('category.products', $category->id) }}">{{ $category->name }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li><a href="{{ route('products.shop') }}">Shop</a></li>
                <li><a href="#">New Arrivals</a></li>
                <li><a href="{{ route('about') }}">About</a></li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
            </ul>
        </div>
    </nav>
</header>
