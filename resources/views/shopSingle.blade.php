<x-master>
    <x-partials.navigation page="item" />
    <div class="site-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{ $product->image }}" alt="Image" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <h2 class="text-black">{{ $product->name }}</h2>
                    <p>{{ $product->description }}</p>
                    <p><strong class="text-primary h4">{{ $product->price }}</strong></p>
                    <div class="mb-1 d-flex">
                        <label for="option-sm" class="d-flex mr-3 mb-3">
                            <span class="d-inline-block mr-2" style="top:-2px; position: relative;"><input
                                    type="radio" id="option-sm" name="shop-sizes"></span> <span
                                class="d-inline-block text-black">Small</span>
                        </label>
                        <label for="option-md" class="d-flex mr-3 mb-3">
                            <span class="d-inline-block mr-2" style="top:-2px; position: relative;"><input
                                    type="radio" id="option-md" name="shop-sizes"></span> <span
                                class="d-inline-block text-black">Medium</span>
                        </label>
                        <label for="option-lg" class="d-flex mr-3 mb-3">
                            <span class="d-inline-block mr-2" style="top:-2px; position: relative;"><input
                                    type="radio" id="option-lg" name="shop-sizes"></span> <span
                                class="d-inline-block text-black">Large</span>
                        </label>
                        <label for="option-xl" class="d-flex mr-3 mb-3">
                            <span class="d-inline-block mr-2" style="top:-2px; position: relative;"><input
                                    type="radio" id="option-xl" name="shop-sizes"></span> <span
                                class="d-inline-block text-black"> Extra Large</span>
                        </label>
                    </div>
                    <div class="mb-5">
                        <div class="input-group mb-3" style="max-width: 120px;">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-primary js-btn-minus" type="button">&minus;</button>
                            </div>
                            <input type="text" class="form-control text-center" value="1" placeholder=""
                                aria-label="Example text with button addon" aria-describedby="button-addon1">
                            <div class="input-group-append">
                                <button class="btn btn-outline-primary js-btn-plus" type="button">&plus;</button>
                            </div>
                        </div>

                    </div>
                    <p><a href="{{ route('cart') }}" class="buy-now btn btn-sm btn-primary">Add To Cart</a></p>

                </div>
            </div>
            @auth
            <div class="row">
                <div class="col-md-12">
                    <h1>Reviews</h1>
                    <form action="{{ route('products.reviews.store', $product->id) }}" method="post">
                        @csrf
                        @method('post')
                        <div class="form-group">
                            <label for="review" class="form-label">Write A Review:</label>
                            {{-- <input type="text" name="body" id="review" class="form-control"> --}}
                            <textarea name="body" id="review" cols="30" rows="5" class="form-control"></textarea>
                            <button type="submit" class="btn btn-primary mt-3">Save</button>
                        </div>
                    </form>
                </div>
            </div>
            @else
            <div class="row">
                <a href="{{ url('/login') }}" class="btn btn-primary">Login to Review</a>
            </div>
            @endauth
            @foreach ($product->reviews as $review)
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title"><strong>{{ $review->reviewedBy->name }}</strong> <em>{{ $review->created_at->diffForHumans() }}</em></div>
                            <div class="card-text">
                                {{ $review->body }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</x-master>
