<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index() {
        $data = User::all();
        return view('components.admin.user.index', compact('data'));
    }

    public function change_role(User $user) {
        $data = Role::all();
        return view('components.admin.user.change_role', compact('data', 'user'));
    }

    public function update_role(User $user, Request $request) {
        Gate::authorize('update-role');

        $user->update([
            'role_id' => $request->role_id
        ]);
        return redirect()->route('users.admin_index');
    }
}
