<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function store(Request $request, Product $product) {
        $dataToBeInserted = [
            'body' => $request->body,
            'reviewed_by' => Auth::id()
        ];

        $product->reviews()->create($dataToBeInserted);

        return redirect()->back();

    }

}
