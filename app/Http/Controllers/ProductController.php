<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->paginate(5);
        return view('shop', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
        return view('shopSingle', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function admin_index() {
        $data = Product::all();
        return view('components.admin.product.index', compact('data'));
    }

    public function admin_create() {
        $categories = Category::all();
        $brands = Brand::all();
        $colors = Color::all();
        return view('components.admin.product.create', compact('categories', 'brands', 'colors'));
    }

    public function admin_store(ProductRequest $request) {
        // dd($request);
        $data = [
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'image' => $this->store_image($request->file('image')),
            'category_id' => $request->category_id,
            'brand_id' => $request->brand_id,
            'color_id' => $request->color_id,
        ];

        Product::create($data);

        return redirect()->route('products.admin_index')->withMessage('Successfully Created!');

    }

    public function admin_show(Product $product) {
        return view('components.admin.product.show', compact('product'));
    }

    public function admin_edit($id) {
        $data = Product::find($id);
        return view('components.admin.product.edit', compact('data'));
    }

    public function admin_update($id, Request $request) {
        $data = Product::find($id);
        $data->update([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
        ]);
        return redirect()->route('products.admin_index')->withMessage('Successfully Updated!');
    }

    public function admin_delete($id) {
        $data = Product::find($id);
        $data->delete();
        return redirect()->route('products.admin_index')->withMessage('Successfully Deleted!');
    }

    public function store_image($image) {
        $originalName = $image->getClientOriginalName();
        $fileName = date('Y-m-d') . time() . $originalName;

        $image->move(storage_path('/app/public/products'), $fileName);

        // Image::make($image)
        //     ->resize(200, 200)
        //     ->save(storage_path() . '/app/public/categories/' . $fileName);

        return $fileName;

    }


}
