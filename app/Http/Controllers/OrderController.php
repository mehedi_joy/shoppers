<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
    public function index() {
        $data = Order::all();
        return view('components.admin.order.index', compact('data'));
    }

    public function create() {
        $products = Product::all();
        return view('components.admin.order.create', compact('products'));
    }

    public function store(Request $request) {
        // dd($request->all());
        $requestData = [
            'address' => $request->address,
        ];

        $order = Order::create($requestData);
        $order->products()->attach($request->products);

        return redirect()->route('orders.admin_index')->withMessage('Added Successfully!');
    }

    public function show(Order $order) {
        return view('components.admin.order.show', compact('order'));
    }
}
