<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    //
    public function categoryWiseProducts(Category $category) {
        $products = $category->products()->latest()->paginate(3);
        return view('shop', compact('products'));
    }

    public function searchProduct(Request $request) {
        // dd($request);
        $products = Product::where('name', 'like', '%'.$request->name.'%')->paginate(5);
        return view('shop', compact('products'));
    }
}
