<?php

use App\Http\Controllers\CategoryAdminController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__.'/auth.php';


Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('categories/{category}/products', [FrontendController::class, 'categoryWiseProducts'])->name('category.products');

Route::post('products/{product}/comments', [ReviewController::class, 'store'])->name('products.reviews.store');
Route::get('products/search', [FrontendController::class, 'searchProduct'])->name('products.search');

Route::get('profile/', [ProfileController::class, 'showProfile'])->name('profile.show');

Route::resource('products', ProductController::class)->names([
    'index' => 'products.shop',
]);



/*
    Frontend TODO
*/
Route::get('/contact', function() {
    return view('contact');
})->name('contact');

Route::get('/cart', function() {
    return view('cart');
})->name('cart');

Route::get('/checkout', function() {
    return view('checkout');
})->name('checkout');

Route::get('/orderConfirmed', function() {
    return view('thankYou');
})->name('thankYou');

Route::get('/about', function() {
    return view('about');
})->name('about');

/*
    Backend Starts
*/

Route::prefix('admin')->group(function () {
    Route::get('/dashboard', function() {
        return view('components.admin.dashboard');
    })->middleware(['auth', 'verified'])->name('admin_dashboard');

    Route::get('/products', [ProductController::class, 'admin_index'
    ])->middleware(['auth', 'verified'])->name('products.admin_index');

    Route::get('/products/create', [ProductController::class, 'admin_create'
    ])->middleware(['auth', 'verified'])->name('products.admin_create');

    Route::post('/products/store', [ProductController::class, 'admin_store'
    ])->middleware(['auth', 'verified'])->name('products.admin_store');

    Route::get('/products/{product}', [ProductController::class, 'admin_show'
    ])->middleware(['auth', 'verified'])->name('products.admin_show');

    Route::get('/products/edit/{id}', [ProductController::class, 'admin_edit'
    ])->middleware(['auth', 'verified'])->name('products.admin_edit');

    Route::patch('/products/edit/{id}', [ProductController::class, 'admin_update'
    ])->middleware(['auth', 'verified'])->name('products.admin_update');

    Route::resource('categories', CategoryAdminController::class)->middleware(['auth', 'verified']);

    Route::prefix('users')->group(function () {
        Route::get('/', [UserController::class, 'index'])->middleware(['auth', 'verified'])->name('users.admin_index');
        Route::get('/edit_role/{user}', [UserController::class, 'change_role'])->middleware(['auth', 'verified'])->name('users.admin_edit_role');
        Route::patch('/update_role/{user}', [UserController::class, 'update_role'])->middleware(['auth', 'verified'])->name('users.admin_update_role');
    });

    Route::prefix('orders')->group(function () {
        Route::get('/', [OrderController::class, 'index'])->middleware(['auth', 'verified'])->name('orders.admin_index');
        Route::get('/create', [OrderController::class, 'create'])->middleware(['auth', 'verified'])->name('orders.admin_create');
        Route::post('/store', [OrderController::class, 'store'])->middleware(['auth', 'verified'])->name('orders.admin_store');
        Route::get('/{order}', [OrderController::class, 'show'])->middleware(['auth', 'verified'])->name('orders.show');
    });


});
