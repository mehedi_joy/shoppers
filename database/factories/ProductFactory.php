<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(5),
            'description' => $this->faker->text(200),
            'image' => $this->faker->imageUrl(480,340,'Product', true),
            'price' => $this->faker->randomFloat(2, 150, 1000),
            'category_id' => $this->faker->numberBetween(1,3),
            'brand_id' => $this->faker->numberBetween(1,3),
            'color_id' => $this->faker->numberBetween(1,3)
        ];
    }
}
