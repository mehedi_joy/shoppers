<?php

namespace Database\Seeders;

use App\Models\Order;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $order1 = Order::create([
            'address' => 'New York, USA'
        ]);

        $order1->products()->attach([1,2]);

        $order2 = Order::create([
            'address' => 'Dubai, United Arab Emarites'
        ]);

        $order2->products()->attach([2,3,4,5]);
    }
}
