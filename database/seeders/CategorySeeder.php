<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Men',
            'image' => 'men.jpg'
        ]);

        Category::create([
            'name' => 'Women',
            'image' => 'women.jpg'
        ]);

        Category::create([
            'name' => 'Children',
            'image' => 'children.jpg'
        ]);
    }
}
