<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['guest', 'maintainer', 'admin'];
        Role::create([
            'name' => $roles[0]
        ]);

        Role::create([
            'name' => $roles[1]
        ]);

        Role::create([
            'name' => $roles[2]
        ]);
    }
}
